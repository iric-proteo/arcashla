################## BASE IMAGE ######################

FROM biocontainers/biocontainers:latest

################## MAINTAINER ######################
MAINTAINER Mathieu Courcelles

USER root

RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
RUN apt-get update && \
    apt-get install -y  \
        git-lfs && \
        apt-get clean && \
        apt-get purge && \
        rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


RUN git lfs install

RUN conda install -c anaconda pigz \
          bedtools \
          kallisto \
          samtools && \
    conda clean --all

RUN conda install python=3 \
                  numpy \
                  pandas \
                  scipy \
                  biopython && \
    conda clean --all



WORKDIR /home/biodocker/bin

RUN git clone https://github.com/RabadanLab/arcasHLA.git

ENV PATH=$PATH:/home/biodocker/bin/arcasHLA

RUN arcasHLA reference --version 3.36.0 && \
        rm -rf /home/biodocker/bin/arcasHLA/dat/IMGTHLA/.git && \
        rm -rf /home/biodocker/bin/arcasHLA/dat/IMGTHLA/pir && \
        rm -rf /home/biodocker/bin/arcasHLA/dat/IMGTHLA/alignments && \
        rm -rf /home/biodocker/bin/arcasHLA/dat/IMGTHLA/msf && \
        rm -rf /home/biodocker/bin/arcasHLA/dat/IMGTHLA/fasta

ENV ARCASHLA_VERSION="arcasHLA v0.2.0, IMGT/HLA v3.36.0"

CMD ["arcasHLA"]
