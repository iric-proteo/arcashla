# arcasHLA container for MAPDP

This repository contains files to build a Docker container to run arcasHLA in MAPDP.
It also contains Docker images in the Packages\Container Registry section.

Documentation and project license can be consulted here: 

* [arcasHLA](https://github.com/RabadanLab/arcasHLA)
* [MAPDP](https://gitlab.com/iric-proteo/mapdp)


## Build instructions

    $ git clone https://gitlab.com/iric-proteo/arcashla
    $ cd arcashla
    $ sudo docker build -t arcashla:build .
    
To use your Docker image in MAPDP, you must push it to your private registry:

    $ sudo docker build -t your_registry/arcashla:build .
    $ sudo docker push your_registry/arcashla:build

## arcasHLA usage

Use the following command to get instruction how to use arcasHLA.

    $ sudo docker run -it --rm -v /home/user/arcashla:/out arcashla:build arcasHLA



Copyright 2015-2020 Mathieu Courcelles

CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses

Pierre Thibault's lab

IRIC - Universite de Montreal